<?php

//  Test task code and  functions 

/**
 * Test task Widget for Footer
 */
require  'wpb_widget.php';


//locate_template('footer.php', true);
locate_template('/template-parts/footer-widget.php', true);
locate_template('/template-parts/site-info.php', true);

/*
  RTL

 *  */

add_action( 'wp_enqueue_scripts', 'my_child_theme_scripts' );
function my_child_theme_scripts() {    
       
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
        
        wp_enqueue_script('task-script', get_theme_file_uri('/js/script.js'), array(), '1.0', true);
}
if (is_rtl()) {
    wp_enqueue_style('rtl-style', get_stylesheet_directory_uri() . '/css/rtl.css', array(), $version);
}


/*
  Woocommerce delete hooks and add  new order hooks
 */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 30);



add_action('init', 'codex_book_init');

/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_book_init() {
    $labels = array(
        'name' => _x('Books', 'post type general name', 'twentyseventeen-child'),
        'singular_name' => _x('Book', 'post type singular name', 'twentyseventeen-child'),
        'menu_name' => _x('Books', 'admin menu', 'twentyseventeen-child'),
        'name_admin_bar' => _x('Book', 'add new on admin bar', 'twentyseventeen-child'),
        'add_new' => _x('Add New', 'book', 'twentyseventeen-child'),
        'add_new_item' => __('Add New Book', 'twentyseventeen-child'),
        'new_item' => __('New Book', 'twentyseventeen-child'),
        'edit_item' => __('Edit Book', 'twentyseventeen-child'),
        'view_item' => __('View Book', 'twentyseventeen-child'),
        'all_items' => __('All Books', 'twentyseventeen-child'),
        'search_items' => __('Search Books', 'twentyseventeen-child'),
        'parent_item_colon' => __('Parent Books:', 'twentyseventeen-child'),
        'not_found' => __('No books found.', 'twentyseventeen-child'),
        'not_found_in_trash' => __('No books found in Trash.', 'twentyseventeen-child')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'twentyseventeen-child'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'book'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('book', $args);
}

add_action('init', 'create_book_taxonomies', 0);

// Create taxonimy for custom post type "book"
function create_book_taxonomies() {

    $labels = array(
        'name' => _x('Genres', 'taxonomy general name'),
        'singular_name' => _x('Genre', 'taxonomy singular name'),
        'search_items' => __('Search Genres'),
        'all_items' => __('All Genres'),
        'parent_item' => __('Parent Genre'),
        'parent_item_colon' => __('Parent Genre:'),
        'edit_item' => __('Edit Genre'),
        'update_item' => __('Update Genre'),
        'add_new_item' => __('Add New Genre'),
        'new_item_name' => __('New Genre Name'),
        'menu_name' => __('Genre'),
    );


    register_taxonomy('genre', array('book'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'genre'),
    ));
}

add_filter('post_updated_messages', 'codex_book_updated_messages');

/**
 * Book update messages.
 *
 * See /wp-admin/edit-form-advanced.php
 *
 * @param array $messages Existing post update messages.
 *
 * @return array Amended post update messages with new CPT update messages.
 */
function codex_book_updated_messages($messages) {
    $post = get_post();
    $post_type = get_post_type($post);
    $post_type_object = get_post_type_object($post_type);

    $messages['book'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => __('Book updated.', 'twentyseventeen-child'),
        2 => __('Custom field updated.', 'twentyseventeen-child'),
        3 => __('Custom field deleted.', 'twentyseventeen-child'),
        4 => __('Book updated.', 'twentyseventeen-child'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf(__('Book restored to revision from %s', 'twentyseventeen-child'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
        6 => __('Book published.', 'twentyseventeen-child'),
        7 => __('Book saved.', 'twentyseventeen-child'),
        8 => __('Book submitted.', 'twentyseventeen-child'),
        9 => sprintf(
                __('Book scheduled for: <strong>%1$s</strong>.', 'twentyseventeen-child'),
                // translators: Publish box date format, see http://php.net/date
                date_i18n(__('M j, Y @ G:i', 'twentyseventeen-child'), strtotime($post->post_date))
        ),
        10 => __('Book draft updated.', 'twentyseventeen-child')
    );

    if ($post_type_object->publicly_queryable && 'book' === $post_type) {
        $permalink = get_permalink($post->ID);

        $view_link = sprintf(' <a href="%s">%s</a>', esc_url($permalink), __('View book', 'twentyseventeen-child'));
        $messages[$post_type][1] .= $view_link;
        $messages[$post_type][6] .= $view_link;
        $messages[$post_type][9] .= $view_link;

        $preview_permalink = add_query_arg('preview', 'true', $permalink);
        $preview_link = sprintf(' <a target="_blank" href="%s">%s</a>', esc_url($preview_permalink), __('Preview book', 'twentyseventeen-child'));
        $messages[$post_type][8] .= $preview_link;
        $messages[$post_type][10] .= $preview_link;
    }

    return $messages;
}

// Register and load the widget
function wpb_load_widget() {
    register_widget('wpb_widget');
}

add_action('widgets_init', 'wpb_load_widget');

function test_widgets_init() {

    register_sidebar(array(
        'name' => __('Footer Task', 'twentyseventeen-child'),
        'id' => 'sidebar-4',
        'description' => __('Add widgets here to appear in your footer.', 'twentyseventeen-child'),
        'before_widget' => '<div class="footer-contact">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="footer-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'test_widgets_init');

function task_scripts() {

    // Test task script
    wp_enqueue_script('task-script-rte', get_theme_file_uri('/js/script.js'), array(), '1.444444', true);
}

add_action('wp_enqueue_scripts', 'task_scripts');


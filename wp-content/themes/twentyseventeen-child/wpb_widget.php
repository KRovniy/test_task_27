<?php

/**
 * Description of wpb_widget
 *
 * @author Konstantin
 */
// Creating the widget 
class wpb_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
// Base ID of your widget
                'wpb_widget',
// Widget name will appear in UI
                __('TestFooter Widget', 'wpb_widget_domain'),
// Widget description
                array('description' => __(' Widget for Footer', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end

    public function widget($args, $instance) {
        $phone = apply_filters('widget_phone', $instance['phone']);
        $email = apply_filters('widget_email', $instance['email']);
        $popup = apply_filters('widget_popup', $instance['popup']);

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
        //echo __($popup, 'wpb_widget_domain');
        ?>

        <div class="footer-info">
            <?php  //print_r($instance); ?>
            <p class="tel">Call Us: <?php echo $phone; ?></p>
            <p class="tel-us"><a href="tel:<?php echo $phone; ?>">Call Us</a></p>
            <p class="email">Email: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
            <p class="email-us"><a href="mailto:<?php echo $email; ?>">Email US</a></p>
            <button class="p-container" onclick="PopUpShow()">Contact Us</button>
        </div>
        <div class="task-popup" id="popup1">
            <div class="task-popup-content">
                <?php echo $popup ."<br>"?>
                <a href="javascript:PopUpHide()">Hide popup</a>
            </div>
        </div>
        <?php
        echo $args['after_widget'];
    }

// Widget Backend 
    public function form($instance) {
        $phone = '';
        if (!empty($instance['phone'])) {
            $phone = $instance['phone'];
        }
        $email = '';
        if (!empty($instance['email'])) {
            $email = $instance['email'];
        }

        $popup = '';
        if (!empty($instance['popup'])) {
            $popup = $instance['popup'];
        }

// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone Number:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email Address:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_name('popup'); ?>"><?php _e('Popup Text:'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('popup'); ?>" name="<?php echo $this->get_field_name('popup'); ?>" type="text" ><?php echo esc_attr($popup); ?></textarea>
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['phone'] = (!empty($new_instance['phone']) ) ? strip_tags($new_instance['phone']) : '';
        $instance['email'] = (!empty($new_instance['email']) ) ? strip_tags($new_instance['email']) : '';
        $instance['popup'] = (!empty($new_instance['popup']) ) ? strip_tags($new_instance['popup']) : '';
       
        return $instance;
        
        
    }

}

// Class wpb_widget ends here
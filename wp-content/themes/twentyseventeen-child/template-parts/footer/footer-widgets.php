<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>

<?php
//die(123);
if (is_active_sidebar('sidebar-2') ||
        is_active_sidebar('sidebar-3') ||
        is_active_sidebar('sidebar-4')) :
    ?>

    <aside class="widget-area" role="complementary">
        <?php if (is_active_sidebar('sidebar-2')) { ?>
            <div class="widget-column footer-widget-1">
                <?php dynamic_sidebar('sidebar-2'); ?>
            </div>
        <?php }
        if (is_active_sidebar('sidebar-3')) {
            ?>
            <div class="widget-column footer-widget-2">
                <?php dynamic_sidebar('sidebar-3'); ?>
            </div>

        <?php } ?>

    </aside><!-- .widget-area -->
    <?php
   // add sidebar-4 after aside 
    if (is_active_sidebar('sidebar-4')) {

        dynamic_sidebar('sidebar-4');
    }

 endif;
